﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.DBContext
{
    public class TaugorPRTContext : DbContext
    {
        public TaugorPRTContext()
            : base("TaugorPRTContext")
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Version> Versions { get; set; }
        public DbSet<Change> Changes { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<RequestMessage> RequestMessages { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Opportunity> Opportunities { get; set; }
        public DbSet<OpportunityInteraction> OpportunityInteractions { get; set; }

        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<City> Cities { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.HasDefaultSchema("Admin");

            //City belongs to a State
            modelBuilder.Entity<City>()
                .HasRequired(c => c.State)
                .WithMany(s => s.Cities)
                .WillCascadeOnDelete(false);

            //State belongs to a Country
            modelBuilder.Entity<State>()
                .HasRequired(s => s.Country)
                .WithMany(c => c.States)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(k => k.Distributors)
                .WithMany(s => s.DistributionCountries)
                .Map(ks =>
                {
                    ks.MapLeftKey("CountryId");
                    ks.MapRightKey("CompanyId");
                    ks.ToTable("Distribution");
                });

            modelBuilder.Entity<Company>()
                .HasRequired(s => s.City)
                .WithMany(c => c.Companies);

            modelBuilder.Entity<Company>()
                .HasOptional(k => k.Admin);

            modelBuilder.Entity<Country>()
                .HasOptional(k => k.Syndicate);

            modelBuilder.Entity<Company>()
                .HasOptional(k => k.Deployer)
                .WithMany(k => k.Deployees);

            modelBuilder.Entity<Company>()
                .HasOptional(k => k.HeadOffice)
                .WithMany(k => k.Branches);

            //User has a parent Company
            modelBuilder.Entity<User>()
               .HasRequired<Company>(u => u.Company)
               .WithMany(c => c.Users);

            //SUBSCRIPTION
            modelBuilder.Entity<User>()
                .HasMany<Product>(u => u.FollowedProducts)
                .WithMany(p => p.Subscriptions)
                .Map(ks =>
                {
                    ks.MapLeftKey("UserRefId");
                    ks.MapRightKey("ProductRefId");
                    ks.ToTable("Subscription");
                });


            //Modules has Base Product with many Products(modules)
            modelBuilder.Entity<Product>()
                        .HasOptional<Product>(s => s.Parent)
                        .WithMany(s => s.Modules);

            //Version has a parent Product
            modelBuilder.Entity<Version>()
                        .HasRequired<Product>(s => s.Product)
                        .WithMany(s => s.Versions);

            //Change has a parent Version
            modelBuilder.Entity<Change>()
                        .HasRequired<Version>(s => s.Version)
                        .WithMany(s => s.Changes);

            //Request has Products
            modelBuilder.Entity<Request>()
                .HasMany<Product>(u => u.Products)
                .WithMany(p => p.Requests)
                .Map(ks =>
                {
                    ks.MapLeftKey("RequestRefId");
                    ks.MapRightKey("ProductRefId");
                    ks.ToTable("RequestProducts");
                });


            //Request has an author
            modelBuilder.Entity<Request>()
                .HasRequired<User>(r => r.Author)
                .WithMany(u => u.Requests)
                .WillCascadeOnDelete(false);

            //Request has an Assigned User
            modelBuilder.Entity<Request>()
                .HasOptional<User>(r => r.AssignedUser)
                .WithMany(u => u.AssignedRequests);

            //Request Message has an Owning Request
            modelBuilder.Entity<RequestMessage>()
                .HasRequired<Request>(rm => rm.Request)
                .WithMany(r => r.Messages);

            //Opportunity has an Assigned User
            modelBuilder.Entity<Opportunity>()
                .HasRequired<User>(r => r.User)
                .WithMany(u => u.Opportunities)
                .WillCascadeOnDelete(false);

            //Opportunity has a Company
            modelBuilder.Entity<Opportunity>()
                .HasRequired<Company>(r => r.Company)
                .WithMany(u => u.Opportunities)
                .WillCascadeOnDelete(false);

            //Opportunity Interaction has an Owning Opportunity
            modelBuilder.Entity<OpportunityInteraction>()
                .HasRequired<Opportunity>(oi => oi.Opportunity)
                .WithMany(o => o.Interactions);
        }


    }
}