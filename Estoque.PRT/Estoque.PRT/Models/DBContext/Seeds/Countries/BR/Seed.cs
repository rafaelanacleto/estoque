﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.DBContext.Seeds.Countries.BR
{
    public static partial class SeedBR
    {
        public static void Seed(DbSet<Country> db)
        {
            var country = new Country()
            { 
                Acronym = "BR",
                Acronym3 = "BRA",
                IsoCode = 76,
                Name = "Brasil",

                States = new HashSet<State>()
                {
                    new State() { Acronym="AC", Name = "Acre", Cities = StateAC.Seed()},
                    new State() { Acronym="AL", Name = "Alagoas", Cities = StateAL.Seed() },
                    new State() { Acronym="AM", Name = "Amazonas", Cities = StateAM.Seed() },
                    new State() { Acronym="AP", Name = "Amapá", Cities = StateAP.Seed() },
                    new State() { Acronym="BA", Name = "Bahia", Cities = StateBA.Seed() },
                    new State() { Acronym="CE", Name = "Ceará", Cities = StateCE.Seed() },
                    new State() { Acronym="DF", Name = "Distrito Federal", Cities = StateDF.Seed() },
                    new State() { Acronym="ES", Name = "Espírito Santo", Cities = StateES.Seed() },
                    new State() { Acronym="GO", Name = "Goiás", Cities = StateGO.Seed() },
                    new State() { Acronym="MA", Name = "Maranhão", Cities = StateMA.Seed() },
                    new State() { Acronym="MG", Name = "Minas Gerais", Cities = StateMG.Seed() },
                    new State() { Acronym="MS", Name = "Mato Grosso do Sul", Cities = StateMS.Seed() },
                    new State() { Acronym="MT", Name = "Mato Grosso", Cities = StateMT.Seed() },
                    new State() { Acronym="PA", Name = "Pará", Cities = StatePA.Seed() },
                    new State() { Acronym="PB", Name = "Paraíba", Cities = StatePB.Seed() },
                    new State() { Acronym="PE", Name = "Pernambuco", Cities = StatePE.Seed() },
                    new State() { Acronym="PI", Name = "Piauí", Cities = StatePI.Seed() },
                    new State() { Acronym="PR", Name = "Paraná", Cities = StatePR.Seed() },
                    new State() { Acronym="RJ", Name = "Rio de Janeiro", Cities = StateRJ.Seed() },
                    new State() { Acronym="RN", Name = "Rio Grande do Norte", Cities = StateRN.Seed() },
                    new State() { Acronym="RO", Name = "Rondônia", Cities = StateRO.Seed() },
                    new State() { Acronym="RR", Name = "Roraima", Cities = StateRR.Seed() },
                    new State() { Acronym="RS", Name = "Rio Grande do Sul", Cities = StateRS.Seed() },
                    new State() { Acronym="SC", Name = "Santa Catarina", Cities = StateSC.Seed() },
                    new State() { Acronym="SE", Name = "Sergipe", Cities = StateSE.Seed() },
                    new State() { Acronym="SP", Name = "São Paulo", Cities = StateSP.Seed() },
                    new State() { Acronym="TO", Name = "Tocantins", Cities = StateTO.Seed() },
                }
            };


            db.Add(country);


        }
    }
}