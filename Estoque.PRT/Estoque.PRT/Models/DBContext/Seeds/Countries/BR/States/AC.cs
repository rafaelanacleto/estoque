﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.DBContext.Seeds.Countries.BR
{
    public static partial class SeedBR
    {
        public static class StateAC
        {
            public static HashSet<City> Seed()
            {
                return new HashSet<City>()
                {
                    new City() { Name="Acrelândia" },
                    new City() { Name="Assis Brasil" },
                    new City() { Name="Brasiléia" },
                    new City() { Name="Bujari" },
                    new City() { Name="Capixaba" },
                    new City() { Name="Cruzeiro do Sul" },
                    new City() { Name="Epitaciolândia" },
                    new City() { Name="Feijó" },
                    new City() { Name="Jordão" },
                    new City() { Name="Mâncio Lima" },
                    new City() { Name="Manoel Urbano" },
                    new City() { Name="Marechal Thaumaturgo" },
                    new City() { Name="Plácido de Castro" },
                    new City() { Name="Porto Acre" },
                    new City() { Name="Porto Walter" },
                    new City() { Name="Rio Branco" },
                    new City() { Name="Rodrigues Alves" },
                    new City() { Name="Santa Rosa do Puru" },
                    new City() { Name="Sena Madureira" },
                    new City() { Name="Senador Guiomard" },
                    new City() { Name="Tarauacá" },
                    new City() { Name="Xapuri" },
                };
            }
        }
    }
    
}