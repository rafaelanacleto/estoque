﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Taugor.PRT.Models.Enums;

namespace Taugor.PRT.Models.DBContext
{
    public class TaugorPRTContextDeploy : DropCreateDatabaseAlways<TaugorPRTContext>
    {
        public override void InitializeDatabase(TaugorPRTContext context)
        {
            base.InitializeDatabase(context);

        }

        protected Version GenerateRoadmap()
        {
            return new Version()
            {
                FullVersion = "0.0.0.0",
                Visibility = Visibility.Public,
                ShortDescription = "Melhorias e correções futuras",
            };
        }
        protected override void Seed(TaugorPRTContext context)
        {
            base.Seed(context);
            Seeds.Countries.BR.SeedBR.Seed(context.Countries);
            context.SaveChanges();
            var nh = context.Cities.FirstOrDefault(k => k.Name == "Novo Hamburgo");
            context.Companies.AddRange(
                new[] { 
                    new Company(){
                        TradingName = "Taugor",
                        CompanyName = "Taugor Corporation",
                        Document = "123",
                        CityId = nh.CityId,
                        Users = new[] {
                            new User()
                            {
                                Name = "Administrador",
                                Username = "admin@taugor.com.br",
                                Password = "Absirwt600",
                                Type = UserType.Admin,
                                Job = "Administrador do Sistema",
                            },
                            new User()
                            {
                                Name = "Desenvolvedor",
                                Username = "desenvolvimento@taugor.com.br",
                                Password = "Absirwt600",
                                Type = UserType.Developer,
                                Job = "Desenvolvedor",
                            },
                            new User()
                            {
                                Name = "Suporte",
                                Username = "suporte@taugor.com.br",
                                Password = "Absirwt600",
                                Type = UserType.Support,
                                Job = "Suporte",
                            },
                            new User()
                            {
                                Name = "Interno",
                                Username = "interno@taugor.com.br",
                                Password = "Absirwt600",
                                Type = UserType.Internal,
                                Job = "Comercial",
                            },
                            new User()
                            {
                                Name = "Parceiro",
                                Username = "partner@taugor.com.br",
                                Password = "Absirwt600",
                                Type = UserType.Partner,
                                Job = "Representante",
                            },
                            new User()
                            {
                                Name = "Cliente",
                                Username = "consumer@taugor.com.br",
                                Password = "Absirwt600",
                                Type = UserType.Consumer,
                                Job = "Analista de Sistemas",
                            },
                        }
                    }
                }
            );


            //Produtos Base
            context.Products.AddRange(
                new[] { 
                    new Product(){
                        Name = "Taugor GED",
                        ShortDescription = "Software GED, DMS e ECM",
                        Description = "Taugor GED é um sistema/software desenvolvida para organizar e gerenciar documentos de forma digital. Seus documentos digitalizados serão armazenados de forma segura e integrada com seu scanner.",
                        Visibility = Visibility.Public,
                        ModuleDescription = "Essentials",
                        Versions = new HashSet<Version>(){
                            GenerateRoadmap(),
                        },
                        Modules = new HashSet<Product>(){
                            new Product(){
                                Name = "Taugor GED - Audit",
                                ShortDescription = "Controle de Auditoria",
                                Description = "Controle de auditoria e blah blah",
                                Visibility = Visibility.Public,
                                ModuleDescription = "Controle de Auditoria",
                                Versions = new HashSet<Version>(){
                                    GenerateRoadmap(),
                                },
                            },
                            new Product(){
                                Name = "Taugor GED - CEDOC",
                                ShortDescription = "Central de Documentos",
                                Description = "Controle de protocolos de documentos",
                                Visibility = Visibility.Public,
                                ModuleDescription = "Controle de protocolos",
                                Versions = new HashSet<Version>(){
                                    GenerateRoadmap(),
                                },
                            }
                        }
                    },
                    new Product(){
                        Name = "Taugor Tractus",
                        ShortDescription = "PMO Automatizado",
                        Description = "Ferramenta PMO completa e auotmiza todo o processo do seu escritório de Projetos, Integrada com Project Server, ferramenta EPM Microsoft. Tenha toda metodologia PMI/Scrum e integrada na sua empresa.",
                        Visibility = Visibility.Public,
                        Versions = new HashSet<Version>(){
                            GenerateRoadmap(),
                        },
                    },
                    new Product(){
                        Name = "Taugor Nuntios",
                        ShortDescription = "Intranet",
                        Description = "Blah Blah",
                        Visibility = Visibility.Public,
                        Versions = new HashSet<Version>(){
                            GenerateRoadmap(),
                        },
                    },
                    new Product(){
                        Name = "Taugor QualiQA",
                        ShortDescription = "Gestão de Qualidade",
                        Description = "Blah blah",
                        Visibility = Visibility.Public,
                        Versions = new HashSet<Version>(){
                            GenerateRoadmap(),
                        },
                    },
                    new Product(){
                        Name = "Taugor GED - Tools",
                        ShortDescription = "Ferramentas do Taugor GED",
                        Description = "Ferramentas do Taugor GED",
                        Visibility = Visibility.Internal,
                        Versions = new HashSet<Version>(){
                            GenerateRoadmap(),
                        },
                    },
                }
                );

        }
    }
}