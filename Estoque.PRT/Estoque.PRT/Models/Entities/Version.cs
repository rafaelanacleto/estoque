﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Web;
using Taugor.PRT.Models;
using Taugor.PRT.Models.Enums;

namespace Taugor.PRT.Models
{
    //[Table("Versions")]
    public partial class Version : ICanHazAttachments
    {  
        public Version()
        {
            Changes = new HashSet<Change>();
        }


        [DisplayName("Código")]
        [Key]
        public int VersionId { get; set; }

        [DisplayName("Codenome")]
        public string Codename { get; set; }

        [DisplayName("Descrição de Desenvolvimento")]
        [DataType(DataType.MultilineText)]
        public string DevelopmentDescription { get; set; }

        [DisplayName("Descrição Interna")]
        [DataType(DataType.MultilineText)]
        public string InternalDescription { get; set; }

        [DisplayName("Resumo")]
        public string ShortDescription { get; set; }

        [DisplayName("Descrição")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Escopo")]
        public Visibility Visibility { get; set; }

        [DisplayName("Produto")]
        public int ProductId { get; set; }

        [DisplayName("Produto")]
        [ForeignKey("ProductId")]
        public Product Product { get; set; }

        [DisplayName("Mudanças")]
        public virtual ICollection<Change> Changes { get; set; }

        [DisplayName("Versão Principal")]
        public int MajorVersion { get; set; }

        [DisplayName("Versão Secundária")]
        public int MinorVersion { get; set; }

        [DisplayName("Build")]
        public int Build { get; set; }

        [DisplayName("Revisão")]
        public int Revision { get; set; }


        [DisplayName("Tipo")]       
        public VersionType Type { get; set; }


        [DisplayName("Data de Publicação")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ReleaseDate { get; set; }

        [DisplayName("Plano de Publicação")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ReleasePlan { get; set; }

        [NotMapped]
        [DisplayName("Versão")]
        public string FullVersionUnderscore
        {
            get { return FullVersion.Replace('.', '_'); }
        }

        [NotMapped]
        [DisplayName("Versão")]
        public virtual string FullVersion
        {
            get
            {
                return IsRoadmap ? "Roadmap" : string.Format("{0}.{1}.{2}.{3}",
                    MajorVersion, MinorVersion, Build, Revision);
            }
            set
            {
                var tokens = value.Split(new char[] { '.' });
                if (tokens.Length == 4)
                {
                    var majorVersion = Convert.ToInt32(tokens[0]);
                    var minorVersion = Convert.ToInt32(tokens[1]);
                    var build = Convert.ToInt32(tokens[2]);
                    var revision = Convert.ToInt32(tokens[3]);

                    MajorVersion = majorVersion;
                    MinorVersion = minorVersion;
                    Build = build;
                    Revision = revision;
                }

            }
        }

        [NotMapped]
        [DisplayName("Código")]
        public virtual string Identifier { get { return string.Format("PV{0:000000000}", VersionId); } }

        [DisplayName("Referência")]
        [NotMapped]
        public virtual string Reference { get { return this.ToString(); } }


        [DisplayName("Referência")]
        [NotMapped]
        public virtual string FileReference
        {
            get
            {
                var version = IsRoadmap ? "Roadmap" : FullVersionUnderscore;
                var productName = Product.Name.TrimStart().TrimEnd().Replace(' ', '_');
                return string.Format("{0}-{1}", productName, version);
            }
        }

        public override string ToString()
        {
            var version = IsRoadmap ? "Roadmap" : FullVersion;
            return string.Format("{0} - {1}", Product.Name, version);
        }

        [DisplayName("Publicada")]
        [NotMapped]
        public virtual bool Released { get { return ReleaseDate.HasValue && ReleaseDate.Value > DateTime.MinValue; } }


        [NotMapped]
        public virtual bool IsRoadmap
        {
            get
            {
                return MajorVersion == 0
                    && MinorVersion == 0
                    && Build == 0
                    && Revision == 0;
            }
        }


    }
}