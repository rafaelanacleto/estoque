﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Taugor.PRT.Models.Enums;

namespace Taugor.PRT.Models
{
    public partial class Company : IEntity
    {
        public Company()
        {
            Opportunities = new HashSet<Opportunity>();
            Users = new HashSet<User>();
            Branches = new HashSet<Company>();
            Deployees = new HashSet<Company>();
        }

        [Key]
        public int CompanyId { get; set; }

        [DisplayName("Documento")]
        public string Document { get; set; }
        
        [DisplayName("Razão Social")]
        public string CompanyName { get; set; }
        
        [DisplayName("Nome Fantasia")]
        public string TradingName { get; set; }

        [DisplayName("Tipo")]
        public CompanyType Type { get; set; }

        [DisplayName("Cidade")]
        public int CityId { get; set; }


        [ForeignKey("CityId")]
        [DisplayName("Cidade")]
        public virtual City City { get; set; }



        [DisplayName("Países(Distribuição)")]
        public virtual ICollection<Country> DistributionCountries { get; set; }

        [DisplayName("Implantações")]
        public ICollection<Company> Deployees { get; set; }

        //[DisplayName("País(Consórcio)")]
        //public virtual Country SyndicateCountry { get; set; }




        [DisplayName("Administrador")]
        public int? AdminId { get; set; }

        [DisplayName("Administrador")]
        [ForeignKey("AdminId")]
        public virtual User Admin { get; set; }


        [DisplayName("Implantadora")]
        public int? DeployerId { get; set; }

        [DisplayName("Implantadora")]
        [ForeignKey("DeployerId")]
        public virtual Company Deployer { get; set; }



        [DisplayName("Matriz")]
        public int? HeadOfficeId { get; set; }

        [DisplayName("Matriz")]
        [ForeignKey("HeadOfficeId")]
        public virtual Company HeadOffice { get; set; }



        [DisplayName("Filiais")]
        public virtual ICollection<Company> Branches { get; set; }

        [DisplayName("Usuários")]
        public virtual ICollection<User> Users { get; set; }

        [DisplayName("Registros de Oportunidade")]
        public virtual ICollection<Opportunity> Opportunities { get; set; }


        [NotMapped]
        [DisplayName("Código")]
        public virtual string Identifier { get { return string.Format("CP{0:000000000}", CompanyId); } }
    }
}