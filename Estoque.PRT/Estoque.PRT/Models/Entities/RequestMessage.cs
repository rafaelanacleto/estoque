﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Taugor.PRT.Models
{
    public partial class RequestMessage : ICanHazAttachments
    {
        [Key]
        public int RequestMessageId { get; set; }

        [DisplayName("Mensagem")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string Message { get; set; }

        [DisplayName("Enviada em")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime Created { get; set; }

        [DisplayName("Autor")]
        public int AuthorId { get; set; }

        [DisplayName("Autor")]
        [ForeignKey("AuthorId")]
        public virtual User Author { get; set; }

        public int RequestId { get; set; }

        [ForeignKey("RequestId")]
        public virtual Request Request { get; set; }

        [NotMapped]
        [DisplayName("Código")]
        public virtual string Identifier { get { return string.Format("RM{0:000000000}", RequestMessageId); } }

    }
}