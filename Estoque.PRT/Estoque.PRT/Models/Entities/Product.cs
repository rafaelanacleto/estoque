﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Web;
using Taugor.PRT.Models.Enums;

namespace Taugor.PRT.Models
{
    //[Table("Products")]
    public partial class Product : ICanHazAttachments
    {
        public Product()
        {
            Modules = new HashSet<Product>();
            Versions = new HashSet<Version>();
            Subscriptions = new HashSet<User>();
            Requests = new HashSet<Request>();
        }

        [DisplayName("Código")]
        [Key]
        public int ProductId { get; set; }

        [DisplayName("Escopo")]
        public Visibility Visibility { get; set; }

        [DisplayName("Nome")]
        [MaxLength(120), MinLength(1)]
        public string Name { get; set; }

        [DisplayName("Resumo")]
        [MaxLength(100)]
        public string ShortDescription { get; set; }

        [DisplayName("Descrição")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }


        [DisplayName("Descrição de Módulo")]
        [MaxLength(120)]
        public string ModuleDescription { get; set; }

        [DisplayName("Produto Base")]
        public int? ParentId { get; set; }

        [DisplayName("É Módulo?")]
        [NotMapped]
        public virtual bool IsModule { get { return ParentId != null; } }

        [ForeignKey("ParentId")]
        [DisplayName("Produto Base")]
        public virtual Product Parent { get; set; }

        [DisplayName("Modulos")]
        public virtual ICollection<Product> Modules { get; set; }

        [DisplayName("Versões")]
        public virtual ICollection<Version> Versions { get; set; }

        [DisplayName("Inscrições")]
        public virtual ICollection<User> Subscriptions { get; set; }

        [DisplayName("Chamados")]
        public virtual ICollection<Request> Requests { get; set; }

        [NotMapped]
        public virtual string ModuleName
        {
            get
            {
                if (IsModule)
                {
                    try
                    {
                        var partial = this.Name.Substring(Parent.Name.Length).TrimStart();
                        var index = partial.IndexOf('-');
                        if(index != -1)
                        {
                            partial = partial.Substring(index+1).TrimStart();
                        }
                        return partial;
                    }
                    catch (Exception ex)
                    {
                        return string.Format("<span data-toggle=\"tooltip\" title =\"{0}\">ERRO</span>", ex.Message);
                    }

                }
                else
                {
                    return "Base";
                }
            }
        }


        [DisplayName("Última Versão")]
        [NotMapped]
        public virtual Version LatestVersion
        {
            get
            {
                return Versions.Where(k =>
                    !(k.MajorVersion == 0
                    && k.MinorVersion == 0
                    && k.Build == 0
                    && k.Revision == 0)
                )
                .OrderByDescending(k => k.MajorVersion)
                .ThenByDescending(k => k.MinorVersion)
                .ThenByDescending(k => k.Build)
                .ThenByDescending(k => k.Revision)
                .FirstOrDefault();
            }
            
        }

        [NotMapped]
        public virtual Version Roadmap
        {
            get
            {
                return Versions.FirstOrDefault(k =>
                    k.MajorVersion == 0
                    && k.MinorVersion == 0
                    && k.Build == 0
                    && k.Revision == 0);
            }

        }


        [NotMapped]
        [DisplayName("Código")]
        public virtual string Identifier { get { return string.Format("MS{0:000000000}", ProductId); } }


    }


}