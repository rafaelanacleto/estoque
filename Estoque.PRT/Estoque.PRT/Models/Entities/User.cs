﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Taugor.PRT.Models.Enums;

namespace Taugor.PRT.Models
{
    //[Table("Users")]
    public partial class User : IEntity
    {
        public User()
        {
            FollowedProducts = new HashSet<Product>();
            Opportunities = new HashSet<Opportunity>();
            Requests = new HashSet<Request>();
            AssignedRequests = new HashSet<Request>();
        }

        [DisplayName("Código")]
        [Key]
        public int UserId { get; set; }

        [DisplayName("Nome")]
        public string Name { get; set; }

        [DisplayName("Sobrenome")]
        public string LastName { get; set; }

        [DisplayName("E-mail")]
        [DataType(DataType.EmailAddress)]
        [NotMapped]
        public string Email { get { return Username; } }

        [DisplayName("Usuário")]
        [DataType(DataType.EmailAddress)]
        public string Username { get; set; }
        
        [DataType(DataType.Password)]
        [DisplayName("Senha")]
        public string Password { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Último Acesso")]
        public DateTime? LastLogin { get; set; }

        [EnumDataType(typeof(UserType))]
        [DisplayName("Tipo")]
        public UserType Type { get; set; }

        [DisplayName("Cargo")]
        public string Job { get; set; }

        [DisplayName("Empresa")]
        public int CompanyId { get; set; }
        
        [ForeignKey("CompanyId")]
        [DisplayName("Empresa")]
        public virtual Company Company { get; set; }

        [DisplayName("Produtos")]
        public virtual ICollection<Product> FollowedProducts { get; set; }
        
        [DisplayName("Registros de Oportunidade")]
        public virtual ICollection<Opportunity> Opportunities { get; set; }

        [DisplayName("Chamados")]
        public virtual ICollection<Request> Requests { get; set; }
        
        [DisplayName("Chamados")]
        public virtual ICollection<Request> AssignedRequests { get; set; }
    }
}