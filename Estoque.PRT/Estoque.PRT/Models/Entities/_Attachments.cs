﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Taugor.PRT.Controllers;
using Taugor.PRT.Models;

namespace Taugor.PRT.Models
{
    internal static class AttachmentHelper
    {
        public static void SetFileExtension(ref string field, string value)
        {
            field = value == null ? null : value.StartsWith(".") ? value.Substring(1) : value;
        }
        public static string GetAttachmentUrl(ICanHazAttachments entity)
        {
            return AttachmentController.GetItemServerRelativePath(entity); 
        }
        
    }

    public partial class RequestMessage : ICanHazAttachments
    {
        [NotMapped]
        public string AttachmentFolder { get { return "Requests/" + Request.Identifier; } }
        [NotMapped]
        public string FileName { get { return string.Format("{0}_{1}.{2}", "RequestMessage", Identifier, FileExtension); } }

        private string _FileExtension;
        [NotMapped]
        public string FileExtension
        {
            get { return _FileExtension; }
            set { AttachmentHelper.SetFileExtension(ref _FileExtension, value); }
        }
        [NotMapped]
        public bool HasAttachment { get { return FileExtension != null; } }
        [NotMapped]
        public string AttachmentUrl { get { return AttachmentHelper.GetAttachmentUrl(this); } }

    }

    public partial class Change : ICanHazAttachments
    {
        [NotMapped]
        public string AttachmentFolder { get { return Version.AttachmentFolder + "/Changes"; } }

        [NotMapped]
        public string FileName { get { return string.Format("{0}_{1}.{2}", "Change", Identifier, FileExtension); } }
        [NotMapped]
        private string _FileExtension;
        [NotMapped]
        public string FileExtension
        {
            get { return _FileExtension; }
            set { AttachmentHelper.SetFileExtension(ref _FileExtension, value); }
        }
        [NotMapped]
        public bool HasAttachment { get { return FileExtension != null; } }
        [NotMapped]
        public string AttachmentUrl { get { return AttachmentHelper.GetAttachmentUrl(this); } }
    }

    public partial class Version : ICanHazAttachments
    {
        [NotMapped]
        public string AttachmentFolder { get { return Product.AttachmentFolder + "/Versions/" + FullVersionUnderscore; } }

        [NotMapped]
        public string FileName { get { return string.Format("{0}.{1}", FileReference, FileExtension); } }

        [NotMapped]
        private string _FileExtension;
        [NotMapped]
        public string FileExtension
        {
            get { return _FileExtension; }
            set { AttachmentHelper.SetFileExtension(ref _FileExtension, value); }
        }
        [NotMapped]
        public bool HasAttachment { get { return FileExtension != null; } }

        [NotMapped]
        public string AttachmentUrl { get { return AttachmentHelper.GetAttachmentUrl(this); } }
    }

    public partial class Product : ICanHazAttachments
    {
        [NotMapped]
        public string AttachmentFolder { get { return "Products/" + Reference; } }
        [NotMapped]
        public string FileName { get { return string.Format("{0}.{1}", Reference.TrimStart().TrimEnd().Replace(' ', '_'), FileExtension); } }

        [NotMapped]
        private string _FileExtension;
        [NotMapped]
        public string FileExtension
        {
            get { return _FileExtension; }
            set { AttachmentHelper.SetFileExtension(ref _FileExtension, value); }
        }

        [NotMapped]
        public bool HasAttachment { get { return FileExtension != null; } }
        [NotMapped]
        public string AttachmentUrl { get { return AttachmentHelper.GetAttachmentUrl(this); } }

    }
}