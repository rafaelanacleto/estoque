﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Taugor.PRT.Models;

namespace Taugor.PRT.Models
{
    public partial class User : IEntity
    {
        public string RedirectTo { get { return "/User"; } }
        public string TypeString { get { return "Users"; } }
        public int ID { get { return UserId; } }
        public string Reference { get { return Username; } }
        public string Key { get { return ID.ToString(); } }
    }

    public partial class Request : IEntity
    {
        public string RedirectTo { get { return "/Request"; } }
        public string TypeString { get { return "Requests"; } }
        public int ID { get { return RequestId; } }
        public string Reference { get { return Identifier; } }
        public string Key { get { return Identifier; } }
    }

    public partial class OpportunityInteraction : IEntity
    {
        public string RedirectTo { get { return "/Opportunity/" + Identifier; } }
        public string TypeString { get { return "OpportunityInteractions"; } }
        public int ID { get { return OpportunityInteractionId; } }
        public string Reference { get { return Identifier; } }
        public string Key { get { return Identifier; } }
    }

    public partial class Opportunity : IEntity
    {
        public string RedirectTo { get { return "/Opportunity"; } }
        public string TypeString { get { return "Opportunities"; } }
        public int ID { get { return OpportunityId; } }
        public string Reference { get { return Identifier; } }
        public string Key { get { return Identifier; } }
    }

    public partial class Company : IEntity
    {
        public string RedirectTo { get { return "/Company"; } }
        public string TypeString { get { return "Companies"; } }
        public int ID { get { return CompanyId; } }
        public string Reference { get { return TradingName; } }
        public string Key { get { return Identifier; } }

    }


    public partial class RequestMessage : ICanHazAttachments
    {
        public string RedirectTo { get { return "/Request/" + Request.Identifier; } }
        public string TypeString { get { return "RequestMessages"; } }
        public int ID { get { return RequestMessageId; } }
        public string Reference { get { return Identifier; } }
        public string Key { get { return Identifier; } }
    }

    public partial class Change : ICanHazAttachments
    {
        public string RedirectTo { get { return "/Change"; } }
        public string TypeString { get { return "Changes"; } }
        public int ID { get { return ChangeId; } }
        public string Reference { get { return Identifier; } }
        public string Key { get { return Identifier; } }
    }

    public partial class Version : ICanHazAttachments
    {
        public string RedirectTo { get { return "/Version"; } }
        public string TypeString { get { return "Versions"; } }
        public int ID { get { return VersionId; } }
        public string Key { get { return ID.ToString(); } }

        //Version already has a definition of Reference
        //public string Reference { get { return Identifier; } }
    }

    public partial class Product : ICanHazAttachments
    {
        public string RedirectTo { get { return "/Product"; } }
        public string TypeString { get { return "Products"; } }
        public int ID { get { return ProductId; } }
        public string Reference { get { return Name; } }
        public string Key { get { return ID.ToString(); } }
    }
}