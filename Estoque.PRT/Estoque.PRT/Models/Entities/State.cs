﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace Taugor.PRT.Models
{
    public class State
    {
        public State()
        {
            Cities = new HashSet<City>();
        }

        [Key]
        public int StateId { get; set; }


        [DisplayName("Nome")]
        public string Name { get; set; }

        [DisplayName("Sigla")]
        public string Acronym { get; set; }

        [DisplayName("País")]
        public int CountryId { get; set; }

        [DisplayName("País")]
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }

        [DisplayName("Cidades")]
        public virtual ICollection<City> Cities { get; set; }
    }
}
