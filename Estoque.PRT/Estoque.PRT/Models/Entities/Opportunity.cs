﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Taugor.PRT.Models.Enums;

namespace Taugor.PRT.Models
{
    public partial class Opportunity : IEntity
    {
        public Opportunity()
        {
            Interactions = new HashSet<OpportunityInteraction>();
        }

        [Key]
        public int OpportunityId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Criada em")]
        public DateTime Created { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [DisplayName("Validade até")]
        public DateTime DueDate { get; set; }

        public int CompanyId { get; set; }
        public int UserId { get; set; }

        [DisplayName("Empresa")]
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        [DisplayName("Responsável")]
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [DisplayName("Situação")]
        public OpportunityState CurrentState { get; set; }

        [DisplayName("Interações")]
        public virtual ICollection<OpportunityInteraction> Interactions { get; set; }

        [NotMapped]
        [DisplayName("Código")]
        public virtual string Identifier { get { return string.Format("OP{0:000000000}", OpportunityId); } }
    }
    
}
