﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Web;
using Taugor.PRT.Models;
using Taugor.PRT.Models.Enums;

namespace Taugor.PRT.Models
{
    //[Table("Changes")]
    public partial class Change : ICanHazAttachments
    {
        [DisplayName("Código")]
        [Key]
        public int ChangeId { get; set; }

        [DisplayName("Descrição de Desenvolvimento")]
        [DataType(DataType.MultilineText)]
        public string DevelopmentDescription { get; set; }

        [DisplayName("Descrição Interna")]
        [DataType(DataType.MultilineText)]
        public string InternalDescription { get; set; }

        [DisplayName("Descrição")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Versão")]
        public int VersionId { get; set; }
        
        [ForeignKey("VersionId")]
        [DisplayName("Versão")]
        public virtual Version Version { get; set; }

        [DisplayName("Tipo")]
        [EnumDataType(typeof(ChangeType))]
        public ChangeType Type { get; set; }

        [DisplayName("Escopo")]
        [EnumDataType(typeof(Visibility))]
        
        public Visibility Visibility { get; set; }

        [DisplayName("Data")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Date { get; set; }

        [NotMapped]
        [DisplayName("Código")]
        public string Identifier { get { return string.Format("CH{0:000000000}", ChangeId); } }


    }
}