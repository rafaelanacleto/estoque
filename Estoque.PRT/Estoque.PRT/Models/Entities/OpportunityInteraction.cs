﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models
{
    public partial class OpportunityInteraction : IEntity
    {
        [Key]
        public int OpportunityInteractionId { get; set; }

        [DisplayName("Oportunidade")]
        public int OpportunityId { get; set; }

        [ForeignKey("OpportunityId")]
        [DisplayName("Oportunidade")]
        public virtual Opportunity Opportunity { get; set; }

        [DisplayName("Descrição")]
        public string Description { get; set; }
        
        [NotMapped]
        [DisplayName("Código")]
        public virtual string Identifier { get { return string.Format("OI{0:000000000}", OpportunityInteractionId); } }
    }
}