﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models
{
    public class Country
    {
        public Country()
        {
            States = new HashSet<State>();
            Distributors = new HashSet<Company>();
        }

        [Key]
        public int CountryId { get; set; }

        [DisplayName("Nome")]
        public string Name { get; set; }

        [DisplayName("Sigla")]
        [StringLength(2)]
        public string Acronym { get; set; }

        [DisplayName("Sigla")]
        [StringLength(3)]
        public string Acronym3 { get; set; }

        [DisplayName("Código")]
        public int IsoCode { get; set; }

        [NotMapped]
        [DisplayName("Código")]
        public int Code { get { return IsoCode; } set { IsoCode = value; } }
       
        [DisplayName("Consórcio")]
        public int? SyndicateId { get; set; }

        [DisplayName("Consórcio")]
        [ForeignKey("SyndicateId")]
        public virtual Company Syndicate { get; set; }



        [DisplayName("Estados")]
        public virtual ICollection<State> States { get; set; }

        [DisplayName("Distribuidores")]
        public virtual ICollection<Company> Distributors { get; set; }

    }
}