﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Taugor.PRT.Models.Enums;

namespace Taugor.PRT.Models
{
    //[Table("Subscriptions")]
    public partial class Request : IEntity
    {
        public Request()
        {
            Products = new HashSet<Product>();
            Messages = new HashSet<RequestMessage>();
        }

        [Key]
        public int RequestId { get; set; }


        [DisplayName("Mensagens")]
        public virtual ICollection<RequestMessage> Messages { get; set; }

        /// <summary>
        /// Request Type MASK
        /// </summary>
        [DisplayName("Tipo")]
        public RequestType Type { get; set; }
       
        [DisplayName("Situação")]
        public RequestState CurrentState { get; set; }

        [DisplayName("Produtos")]
        public virtual ICollection<Product> Products { get; set; }

        [DisplayName("Autor")]
        public int AuthorId { get; set; }
        
        [DisplayName("Autor")]
        [ForeignKey("AuthorId")]
        public virtual User Author { get; set; }

        [DisplayName("Responsável")]
        public int? AssignedUserId { get; set; }

        [DisplayName("Responsável")]
        [ForeignKey("AssignedUserId")]
        public virtual User AssignedUser { get; set; }

        [DisplayName("Criada em")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public DateTime Created { get; set; }


        [NotMapped]
        [DisplayName("Última atualização em")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy HH:mm}")]
        public virtual DateTime LastUpdate { get { return Messages.Count == 0 ? Created : Messages.Last().Created; } }

        [NotMapped]
        [DisplayName("Código")]
        public virtual string Identifier { get { return string.Format("RQ{0:000000000}", RequestId); } }
    }

}