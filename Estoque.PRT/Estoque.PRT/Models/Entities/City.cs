﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Taugor.PRT.Models
{
    public class City
    {
        public City()
            : base()
        {
            Companies = new HashSet<Company>();
        }

        [Key]
        public int CityId { get; set; }

        [DisplayName("Nome")]
        public string Name { get; set; }

        [DisplayName("Estado")]
        public int StateId { get; set; }

        [DisplayName("Estado")]
        [ForeignKey("StateId")]
        public virtual State State { get; set; }

        [DisplayName("Empresas")]
        public virtual ICollection<Company> Companies { get; set; }
    }
}
