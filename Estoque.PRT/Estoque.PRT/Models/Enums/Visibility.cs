﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.Enums
{
    public enum Visibility
    {
        [Description("Público")]
        Public,

        [Description("Interno")]
        Internal,

        [Description("Desenvolvimento")]
        Development,
    }
}