﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.Enums
{
    public enum UserType
    {
        [Description("Administrador")]
        Admin,
        [Description("Desenvolvedor")]
        Developer,
        [Description("Suporte")]
        Support,
        [Description("Interno")]
        Internal,
        [Description("Parceiro")]
        Partner,
        [Description("Cliente")]
        Consumer,
    }
}