﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.Enums
{
    public enum OpportunityState
    {
        [Description("Aberta")]
        Open,
        [Description("Vencida")]
        Expired,
        [Description("Cancelada")]
        Cancelled,
        [Description("Ganha")]
        Won,
        [Description("Perdida")]
        Lost,

    }
}