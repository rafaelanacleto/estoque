﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.Enums
{
    public enum ChangeType
    {
        [Description("Outro")]
        Other,

        [Description("Novo Recurso")]
        NewFeature,

        [Description("Correção")]
        Fix,

        [Description("Melhoria")]
        Improvement,
    }


}