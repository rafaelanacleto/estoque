﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.Enums
{
    public enum RequestState
    {
        [Description("Não Iniciado")]
        NotStarted,
        [Description("Em atendimento")]
        Running,
        [Description("Concluído")]
        Finished,
        [Description("Cancelado pelo autor")]
        CancelledByUser,
        [Description("Cancelado")]
        CancelledBySupport,

    }

    [Flags]
    public enum RequestType
    {
        [Description("Outro")]
        Other = 1 << 0,

        [Description("Dúvida")]
        Doubt = 1 << 1,

        [Description("Novo Recurso")]
        NewFeature = 1 << 2,

        [Description("Melhoria")]
        Improvement = 1 << 3,

        [Description("Sugestão")]
        Suggestion = 1 << 4,

        [Description("Problema - Ambiente de Homologação")]
        ProblemQA = 1 << 5,

        [Description("Problema - Ambiente de Produção")]
        Problem = 1 << 6,

        [Description("Correção de bug - Homologação")]
        BugFixQA = 1 << 7,

        [Description("Correção de bug - Produção")]
        BugFix = 1 << 8,
    }
}