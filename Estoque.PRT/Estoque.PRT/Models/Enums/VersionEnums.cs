﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.Enums
{
    public enum VersionType
    {
        [Description("Outro")]
        Other,

        [Description("Desenvolvimento")]
        Development,

        [Description("Integração")]
        Integration,

        [Description("Qualidade/Teste")]
        QualityAssurance,

        [Description("Pré-produção")]
        Stage,

        [Description("Produção")]
        Production,

        [Description("Versão com Erro")]
        Error,
    }    
}