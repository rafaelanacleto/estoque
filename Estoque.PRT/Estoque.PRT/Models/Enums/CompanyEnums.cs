﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.Enums
{
    public enum CompanyType
    {
        [Description("Outro")]
        Other,
        [Description("Aberta")]
        Lead,
        [Description("Cliente")]
        Consumer,
        [Description("Bureau")]
        Bureau,
        [Description("Canal - Representante")]
        RepresentativeChannel,
        [Description("Canal - Franquia")]
        FranchiseChannel,
        [Description("Distribuidor")]
        Distributor,
    }
}