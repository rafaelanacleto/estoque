﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models
{
    public interface IViewModel<T> where T : IEntity
    {
        void MapFrom(T entity);
        void MapTo(T entity);
    }
}