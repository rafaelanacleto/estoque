﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Taugor.PRT.Models
{
    public interface ICanHazAttachments : IEntity
    {
        string AttachmentFolder { get; }
        string FileExtension { get; set; }
        string FileName { get; }
        bool HasAttachment { get; }
        string AttachmentUrl { get; }


        //string GetAttachmentFolder();
        //void SetFileExtension(string fileName);
        //string GetFileName();
    }
}
