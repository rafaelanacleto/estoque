﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models
{
    public interface IEntity
    {
        string RedirectTo { get; }
        string TypeString { get; }
        string Reference { get; }
        string Key { get; }
        int ID { get; }
        
    }
}