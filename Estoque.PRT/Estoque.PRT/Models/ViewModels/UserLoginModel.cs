﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.ViewModels
{
    public class UserLoginVM
    {
        
        [DisplayName("Usuário")]
        [DataType(DataType.EmailAddress)]
        public string Username { get; set; }
        
        [DisplayName("Senha")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DisplayName("Manter-me conectado")]
        public bool RememberMe { get; set; }
        
    }
}