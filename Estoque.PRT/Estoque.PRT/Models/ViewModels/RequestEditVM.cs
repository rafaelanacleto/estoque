﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Taugor.PRT.Models.DataAnnotations;
using Taugor.PRT.Models.Enums;

namespace Taugor.PRT.Models.ViewModels
{
    public class RequestEditVM : BaseViewModel<Request>, IViewModel<Request>
    {
        public RequestEditVM() { }
        public RequestEditVM(Request entity) : base(entity) { }

        [ViewModelMapping(MappingDirection.From)]
        public int RequestId { get; set; }

        [DisplayName("Situação")]
        public RequestState CurrentState { get; set; }

        [DisplayName("Responsável")]
        public int? AssignedUserId { get; set; }
    }
}
