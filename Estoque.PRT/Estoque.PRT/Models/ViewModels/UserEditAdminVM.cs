﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Taugor.PRT.Models.DataAnnotations;
using Taugor.PRT.Models.Enums;

namespace Taugor.PRT.Models.ViewModels
{
    public class UserEditAdminVM : BaseViewModel<User>, IViewModel<User>
    {
        public UserEditAdminVM() { }
        public UserEditAdminVM(User entity) : base(entity) { }

        [ViewModelMapping(MappingDirection.From)]
        public int UserId { get; set; }

        [DisplayName("Nome")]
        public string Name { get; set; }

        [DisplayName("Sobrenome")]
        public string LastName { get; set; }

        [DisplayName("E-mail")]
        [DataType(DataType.EmailAddress)]
        [ViewModelMapping(MappingDirection.From)]
        public string Email { get; set; }

        [DisplayName("Cargo")]
        public string Job { get; set; }

        [EnumDataType(typeof(UserType))]
        [DisplayName("Tipo")]
        public UserType Type { get; set; }
        
    }
}
