﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Taugor.PRT.Models.DataAnnotations;

namespace Taugor.PRT.Models.ViewModels
{
    public class UserEditVM : BaseViewModel<User>, IViewModel<User>
    {
        public UserEditVM() { }
        public UserEditVM(User entity) : base(entity) { }

        [ViewModelMapping(MappingDirection.From)]
        public int UserId { get; set; }

        [DisplayName("Nome")]
        public string Name { get; set; }

        [DisplayName("Sobrenome")]
        public string LastName { get; set; }

        [DisplayName("E-mail")]
        [DataType(DataType.EmailAddress)]
        [ViewModelMapping(MappingDirection.From)]
        public string Email { get; set; }

        [DisplayName("Cargo")]
        public string Job { get; set; }

        
    }
}
