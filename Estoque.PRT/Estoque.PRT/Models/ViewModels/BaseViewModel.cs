﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;
using Taugor.PRT.Models.DataAnnotations;

namespace Taugor.PRT.Models.ViewModels
{
    public abstract class BaseViewModel<T> : IViewModel<T> where T : IEntity
    {
        public BaseViewModel()
        {
        }
        public BaseViewModel(T entity)
        {
            MapFrom(entity);
        }

        protected virtual void Map(T entity, Action<PropertyInfo, T, MappingDirection> action)
        {
            if (entity != null)
            {

                foreach (var prop in this.GetType().GetProperties())
                {
                    var attributes = prop.GetCustomAttributes(typeof(ViewModelMappingAttribute), false);
                    var direction = attributes.Length == 0 
                        ? MappingDirection.Both
                        : ((ViewModelMappingAttribute)attributes[0]).Direction;

                    action(prop, entity, direction);
                }
            }
        }

        public virtual void MapFrom(T entity)
        {
            var action = new Action<PropertyInfo, T, MappingDirection>(
                (vmp, e, d) =>
                {
                    if (d == MappingDirection.Both || d == MappingDirection.From)
                    {
                        vmp.SetValue(this, typeof(T).GetProperty(vmp.Name).GetValue(entity));
                    }
                    
                });
            Map(entity, action);
        }

        public virtual void MapTo(T entity)
        {
            var action = new Action<PropertyInfo, T, MappingDirection>(
                (vmp, e, d) =>
                {
                    if (d == MappingDirection.Both || d == MappingDirection.To)
                    {
                        typeof(T).GetProperty(vmp.Name).SetValue(e, vmp.GetValue(this));
                    }
                });
            Map(entity, action);
        }
    }
}