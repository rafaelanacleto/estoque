﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Taugor.PRT.Models.DataAnnotations;
using Taugor.PRT.Models.Enums;
using Taugor.PRT.Util;

namespace Taugor.PRT.Models.ViewModels
{
    public class RequestCreateVM : BaseViewModel<Request>, IViewModel<Request>
    {
        public RequestCreateVM()
        {
            ProductIds = new List<int>();
            TypeIds = new List<int>();
        }

        public int RequestMessageId { get; set; }

        [AllowHtml]
        [DisplayName("Mensagem")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage="A asfhksajhfkajsfhkjash")]
        [MinLength(10, ErrorMessage = "A mensagem não pode conter menos que 10 carácteres")]
        [MaxLength(500, ErrorMessage="A mensagem não pode conter mais do que 500 carácteres")]
        public string Message { get; set; }

        [DisplayName("Autor")]
        public int AuthorId { get; set; }
       
        [DisplayName("Produto(s)")]
        [EnsureMinimumElements(1, ErrorMessage = "O chamado deve conter pelo menos 1 produto")]
        public List<int> ProductIds { get; set; }

        [DisplayName("Tipo(s)")]
        [EnsureMinimumElements(1, ErrorMessage="O chamado deve conter pelo menos 1 tipo")]
        public List<int> TypeIds { get; set; }

        public override void MapFrom(Request entity)
        {
        }        
        public override void MapTo(Request entity)
        {
            entity.AuthorId = AuthorId;
            TypeIds.ForEach(k => entity.Type |= (RequestType)k);
            entity.Messages.Add(new RequestMessage()
            {
                AuthorId = AuthorId,
                Created = entity.Created,
                Message = HtmlTagsHelper.FilterMessage(Message)
            });
        }

    }
}
