﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ViewModelMappingAttribute : Attribute
    {
        public MappingDirection Direction { get; set; }
        public ViewModelMappingAttribute(MappingDirection d)
        {
            Direction = d;
        }
    }
    public enum MappingDirection
    {

        /// <summary>
        /// Gets and sets model's value
        /// </summary>
        Both,
        /// <summary>
        /// Don't get or set model's value
        /// </summary>
        None,
        /// <summary>
        /// Gets model's value
        /// </summary>
        From,
        /// <summary>
        /// Sets model's value
        /// </summary>
        To,
        
    }
}