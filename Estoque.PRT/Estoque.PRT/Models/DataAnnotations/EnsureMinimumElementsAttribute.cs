﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Models.DataAnnotations
{
    public class EnsureMinimumElementsAttribute : ValidationAttribute
    {
        public int MinElements { get; private set; }
        public EnsureMinimumElementsAttribute(int minElements)
        {
            MinElements = minElements;
        }

        public override bool IsValid(object value)
        {
            var list = value as IList;
            if (list != null)
            {
                return list.Count >= MinElements;
            }
            return false;
        }
    }
}