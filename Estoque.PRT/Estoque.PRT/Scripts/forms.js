﻿$(document).ready(
    function(){
        $("input[type!='hidden'][type!='checkbox'][type!='radio']").addClass("form-control");
        $("select").addClass("form-control");
        $("textarea").addClass("form-control");

        $("input[type!='submit'].form-control").first().putCursorAtEnd();
    }
);

