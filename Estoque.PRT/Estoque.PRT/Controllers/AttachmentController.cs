﻿using System;
using System.Collections.Generic;
using System.IO;
using IO = System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using Taugor.PRT.Models.DBContext;
using Taugor.PRT.Models;
using Taugor.PRT.Util;


namespace Taugor.PRT.Controllers
{

    public class AttachmentController : Controller
    {
        private TaugorPRTContext db = new TaugorPRTContext();
        public static readonly string UploadFolder = "~/Content/uploads";


        [Authorize]
        public ActionResult Download(string type, int itemId)
        {
            var prop = typeof(TaugorPRTContext).GetProperty(type);
            dynamic set = prop.GetValue(db);
            var dItem = set.Find(itemId);
            var item = (ICanHazAttachments)dItem;

            if (item != null)
            {
                var path = GetPath(item);
                if(IO.File.Exists(path))
                {
                    var content = IO.File.ReadAllBytes(path);
                    return File(content, MimeTypes.GetMimeType(Path.GetExtension(item.FileName)), item.FileName);
                }
                else
                {
                    return HttpNotFound("Arquivo não encontrado");
                }
            }
            else
            {
                return HttpNotFound("Item não encontrado");
            }
        }


        [Authorize]
        public ActionResult Upload(string type, int itemId)
        {           
            var prop = typeof(TaugorPRTContext).GetProperty(type);                      
            dynamic set = prop.GetValue(db);            
            var dItem = set.Find(itemId);
            var item = (ICanHazAttachments)dItem;

            if (item != null)
            {
                return View(item);
            }
            else
            {
                return HttpNotFound("Item não encontrado");
            }
        }


        [HttpPost]
        [Authorize]
        public ActionResult Upload(string type, int itemId, HttpPostedFileBase file)
        {
            var prop = typeof(TaugorPRTContext).GetProperty(type);
            dynamic set = prop.GetValue(db);
            var dItem = set.Find(itemId);
            var item = (ICanHazAttachments)dItem;

            if (file.ContentLength > 0 && item != null)
            {
                SaveFile(item, file);
            }

            return RedirectPermanent(item.RedirectTo);

        }

        public void SaveFile(ICanHazAttachments item, HttpPostedFileBase file)
        {
            if (file.ContentLength > 0 && item != null)
            {                
                item.FileExtension = Path.GetExtension(file.FileName);
                db.SaveChanges();
                var path = GetPath(item);
                SaveFile(file, path);
            }
        }

        //public File GetFile(ICanHazAttachments item)
        //{
        //    var path = GetPath(item);
        //    return null;
        //}

        public string GetPath(ICanHazAttachments item)
        {
            var path = Path.Combine(Server.MapPath("~/Content/uploads/"), item.AttachmentFolder);
            path = Path.Combine(path, item.FileName);
            return path;
        }


        public static string GetItemServerRelativePath(ICanHazAttachments item)
        {
            var path = Path.Combine(UploadFolder, item.AttachmentFolder);
            path = Path.Combine(path, item.FileName);
            return path;
        }


        public void SaveFile(HttpPostedFileBase file, string path)
        {
            if (file.ContentLength > 0)
            {
                file.SaveAs(path);
                //+ Path.GetExtension(file.FileName)
            }
        }

        [Authorize]
        public ActionResult Delete(string type, int itemId)
        {
            if (type == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var prop = typeof(TaugorPRTContext).GetProperty(type);
            dynamic set = prop.GetValue(db);
            var dItem = set.Find(itemId);
            var item = (ICanHazAttachments)dItem;

            if (item != null)
            {
                var path = GetPath(item);
                if (IO.File.Exists(path))
                {
                    return View(item);
                }
                else
                {
                    return HttpNotFound("Arquivo não encontrado");
                }
            }
            else
            {
                return HttpNotFound("Item não encontrado");
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(string type, int itemId)
        {
            var prop = typeof(TaugorPRTContext).GetProperty(type);
            dynamic set = prop.GetValue(db);
            var dItem = set.Find(itemId);
            var item = (ICanHazAttachments)dItem;

            if (item != null)
            {
                var path = GetPath(item);
                IO.File.Delete(path);
                item.FileExtension = null;
                db.SaveChanges();
                
            }
            return Redirect(item.RedirectTo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}