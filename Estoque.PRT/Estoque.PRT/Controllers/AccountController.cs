﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Taugor.PRT.Models.DBContext;
using Taugor.PRT.Models.ViewModels;

namespace Taugor.PRT.Controllers
{
    public class AccountController : Controller
    {
        private TaugorPRTContext db = new TaugorPRTContext();

        //
        // GET: /Account/
        public ActionResult Login()
        {
            return this.View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLoginVM model, string returnUrl)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            var user = db.Users.FirstOrDefault(k => k.Username == model.Username && k.Password == model.Password);
            
            if(user != null)
            {
                FormsAuthentication.SetAuthCookie(model.Username, model.RememberMe);
                if (this.Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                {
                    return this.Redirect(returnUrl);
                }

                return this.RedirectToAction("Index", "Home");
            }

            this.ModelState.AddModelError(string.Empty, "The user name or password provided is incorrect.");

            return this.View(model);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return this.RedirectToAction("Index", "Home");
        }
	}
}