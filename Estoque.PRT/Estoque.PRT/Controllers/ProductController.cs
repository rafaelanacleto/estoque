﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taugor.PRT.Models.DBContext;
using Taugor.PRT.Models;

namespace Taugor.PRT.Controllers
{
    public class ProductController : Controller
    {
        private TaugorPRTContext db = new TaugorPRTContext();

        // GET: /Product/
        public async Task<ActionResult> Index(int? id = null)
        {
            await db.Products.LoadAsync();
            ViewBag.Context = db;
            if (id.HasValue)
            {
                return View(await db.Products
                    .Where(k => k.ProductId == id && k.ParentId != id)
                    .ToListAsync());

            }
            else
            {
                return View(await db.Products
                    .Where(k => k.ParentId == null)
                    .ToListAsync());
            }

        }

        public async Task<ActionResult> Modules(int id)
        {
            await db.Products.LoadAsync();
            ViewBag.Context = db;

            return View(await db.Products
                .Where(k => k.ProductId == id || k.ParentId == id)
                .ToListAsync());


        }

        // GET: /Product/Roadmap/5
        public async Task<ActionResult> Roadmap(int? id)
        {
            ViewBag.Context = db;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }



        // GET: /Product/Create
        [Authorize]
        public ActionResult Create(int? BaseId = null)
        {
            var product = db.Products.Find(BaseId);

            if (BaseId.HasValue)
            {
                ViewBag.ParentId = new SelectList(db.Products, "ProductId", "Name", BaseId);
            }
            else
            {
                ViewBag.ParentId = new SelectList(db.Products, "ProductId", "Name");
            }

            if (product != null)
            {
                return View(new Product() { ParentId = BaseId, Name = product.Name + " - " });
            }
            else
            {
                return View();
            }

        }

        // POST: /Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Create([Bind(Include = "ProductId,Name,ShortDescription,Description,ParentId")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ParentId = new SelectList(db.Products, "ProductId", "Name", product.ParentId);
            return View(product);
        }

        // GET: /Product/Edit/5
        [Authorize]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentId = new SelectList(db.Products, "ProductId", "Name", product.ParentId);

            return View(product);
        }

        // POST: /Product/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Edit([Bind(Include = "ProductId,Name,ShortDescription,Description,ParentId")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ParentId = new SelectList(db.Versions, "ProductId", "Name", product.ParentId);
            return View(product);
        }

        // GET: /Product/Delete/5
        [Authorize]
        public async Task<ActionResult> Delete(int? id)
        {
            ViewBag.Context = db;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.Products.FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: /Product/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Product product = await db.Products.FindAsync(id);
            db.Products.Remove(product);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
