﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Data.SqlTypes;
using System.Text.RegularExpressions;
using Taugor.PRT.Models.Enums;
using Taugor.PRT.Models;
using Taugor.PRT.Util;
using Taugor.PRT.Models.ViewModels;
using Taugor.PRT.Models.DBContext;

namespace Taugor.PRT.Controllers
{
    public class RequestController : Controller
    {
        private TaugorPRTContext db = new TaugorPRTContext();

        // GET: /Request/
        // GET: /Request/RQ000000001

        [Authorize]
        public async Task<ActionResult> Index(RequestState? state, int? author, int? assigned, int? company)
        {
            if (Request.IsAuthenticated)
            {
                var user = db.Users.Where(k => k.Username == User.Identity.Name).FirstOrDefault();
                if (user == null)
                {
                    return RedirectToAction("LogOff", "Account");
                }
                                
                ViewBag.authorId = author;
                ViewBag.assignedId = assigned;
                ViewBag.companyId = company;

                ViewBag.state = state;
                ViewBag.Author = author.HasValue ? await db.Users.FindAsync(author) : null;
                ViewBag.Assigned = assigned.HasValue ? await db.Users.FindAsync(assigned) : null;
                ViewBag.Company = company.HasValue ? await db.Companies.FindAsync(company) : null;

                if (user.Type <= UserType.Support)
                {
                    var requests = await GetRequests(state, author, assigned);
                    return View(requests);
                }
                else //if (user.Type <= UserType.Consumer)
                {
                    return View(await GetRequests(state, user.ID, assigned));
                    //return RedirectToAction("Create");
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        private async Task<List<Request>> GetRequests(RequestState? state, int? author, int? assigned)
        {
            var requests = await db.Requests.ToListAsync();
            if (author.HasValue) { requests = requests.Where(k => k.AuthorId == author.Value).ToList(); }
            if (assigned.HasValue) { requests = requests.Where(k => k.AssignedUserId == assigned.Value).ToList(); }
            if (state.HasValue) { requests = requests.Where(k => k.CurrentState == state.Value).ToList(); }

            return requests;
        }
        // GET: /Request/Details/RQ000000001
        [Authorize]
        public async Task<ActionResult> Details(string identifier)
        {
            var id = Convert.ToInt32(identifier.Substring(3));
            if (identifier == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Request request = await db.Requests.FindAsync(id);
            if (request == null)
            {
                return HttpNotFound();
            }
            return View(request);
        }

        // GET: /Request/Take/RQ000000001
        [Authorize]
        public async Task<ActionResult> TakeRequest(string identifier)
        {
            var id = Convert.ToInt32(identifier.Substring(3));
            if (identifier == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = db.Users.Where(k => k.Username == User.Identity.Name).FirstOrDefault();
            Request request = await db.Requests.FindAsync(id);
            if (request == null)
            {
                return HttpNotFound();
            }

            if (request.AssignedUser != null && request.AssignedUser.Type >= user.Type
                || request.AssignedUser == null)
            {
                request.AssignedUserId = user.ID;
                request.CurrentState = RequestState.Running;
                await db.SaveChangesAsync();
            }
            return RedirectToActionPermanent("index");
        }

        // GET: /Request/Create
        public ActionResult Create()
        {
            var user = db.Users.Where(k => k.Username == User.Identity.Name).FirstOrDefault();
            if (user == null)
            {
                return RedirectToAction("LogOff", "Account");
            }

            if (user.Type <= UserType.Internal)
            {
                ViewBag.Products = db.Products.Select(k => new SelectListItem() { Text = k.Name, Value = k.ProductId.ToString() });
            }
            else if (user.Type == UserType.Partner)
            {
                ViewBag.Products = db.Products.Where(k => k.Visibility == Visibility.Internal || k.Visibility == Visibility.Public).Select(k => new SelectListItem() { Text = k.Name, Value = k.ProductId.ToString() });
            }
            else if (user.Type == UserType.Consumer)
            {
                ViewBag.Products = db.Products.Where(k => k.Visibility == Visibility.Internal || k.Visibility == Visibility.Public).Select(k => new SelectListItem() { Text = k.Name, Value = k.ProductId.ToString() });
            }

            ViewBag.Type = Enum.GetValues(typeof(RequestType))
                             .Cast<RequestType>()
                             .Select(c => new SelectListItem
                             {
                                 Text = EnumHelper.GetDescriptionOrDefault(c),
                                 Value = ((int)c).ToString()
                             });

            return View(new RequestCreateVM() { AuthorId = user.UserId });
        }

        // POST: /Subscription/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Create(RequestCreateVM model)
        {
            var now = DateTime.Now;
            var request = new Request(){
                Created = now,
            };
            model.MapTo(request);

            model.ProductIds.ForEach(
                id =>
                {
                    var product = db.Products.Find(id);
                    if (product != null)
                    {
                        request.Products.Add(product);
                    }
                }
            );            

            if (ModelState.IsValid)
            {
                db.Requests.Add(request);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return new HttpStatusCodeResult(HttpStatusCode.NoContent);
        }

        [Authorize]
        public async Task<ActionResult> Reply(string identifier)
        {
            var id = Convert.ToInt32(identifier.Substring(3));
            if (identifier == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Request request = await db.Requests.FindAsync(id);
            if (request == null)
            {
                return HttpNotFound();
            }

            var user = db.Users.Where(k => k.Username == User.Identity.Name).FirstOrDefault();
            if (user == null)
            {
                return RedirectToAction("LogOff", "Account");
            }

            if (user.Type <= UserType.Support || user.UserId == request.AuthorId)
            {
                return View(
                    new RequestMessage()
                    {
                        Request = request,
                        RequestId = request.RequestId,
                        AuthorId = user.UserId,
                    }
                    );
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Reply(RequestMessage model)
        {
            var now = DateTime.Now;
            model.Message = HtmlTagsHelper.FilterMessage(model.Message);

            if (ModelState.IsValid)
            {
                model.Created = now;
                db.RequestMessages.Add(model);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return RedirectToActionPermanent("Details", new { identifier = model.Request.Identifier });
        }

        public async Task<ActionResult> Edit(string identifier)
        {
            var id = Convert.ToInt32(identifier.Substring(3));
            if (identifier == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Request request = await db.Requests.FindAsync(id);
            if (request == null)
            {
                return HttpNotFound();
            }

            var user = db.Users.Where(k => k.Username == User.Identity.Name).FirstOrDefault();
            if (user == null)
            {
                return RedirectToAction("LogOff", "Account");
            }

            if (user.Type <= UserType.Support)
            {
                ViewBag.Users = UserController.GetSupportUsers();
                return View(new RequestEditVM(request));
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Edit(RequestEditVM viewModel)
        {
            if (ModelState.IsValid)
            {
                Request request = await db.Requests.FindAsync(viewModel.RequestId);
                viewModel.MapTo(request);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        

        //// GET: /Subscription/Edit/5
        //public async Task<ActionResult> Edit(string email)
        //{
        //    if (email == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Subscription subscription = await db.Subscriptions.FindAsync(email);
        //    if (subscription == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    if (Request.IsAuthenticated)
        //    {
        //        var user = db.Users.Where(k => k.Username == User.Identity.Name).FirstOrDefault();
        //        if (user == null)
        //        {
        //            return RedirectToAction("LogOff", "Account");
        //        }

        //        switch (user.Type)
        //        {
        //            case UserType.Developer:
        //                ViewBag.Products = db.Products.Select(k => new SelectListItem() { Text = k.Name, Value = k.ProductId.ToString() });
        //                break;
        //            case UserType.Internal:
        //                ViewBag.Products = db.Products.Select(k => new SelectListItem() { Text = k.Name, Value = k.ProductId.ToString() });
        //                break;
        //            case UserType.Partner:
        //                ViewBag.Products = db.Products.Where(k => k.Visibility == Visibility.Internal || k.Visibility == Visibility.Public).Select(k => new SelectListItem() { Text = k.Name, Value = k.ProductId.ToString() });
        //                break;
        //        }

        //    }
        //    else
        //    {
        //        ViewBag.Products = db.Products.Where(k => k.Visibility == Visibility.Public)
        //            .Select(k => new SelectListItem() { Text = k.Name, Value = k.ProductId.ToString() });
        //    }

        //    return View(subscription);
        //}

        //// POST: /Subscription/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "SubscriptionId,Email")] Subscription subscription)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(subscription).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(subscription);
        //}


        //// GET: /Subscription/Delete/5
        //public async Task<ActionResult> Unsubscribe(string email, string token)
        //{
        //    if (email == null || token == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Subscription subscription = await db.Subscriptions.FindAsync(email);
        //    if (subscription == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    if (subscription.Token == token)
        //    {
        //        return View(subscription);
        //    }
        //    else
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "Token de validação inválido");
        //    }

        //}

        //// POST: /Subscription/Delete/5
        //[HttpPost, ActionName("Unsubscribe")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> UnsubscribeConfirmed(string email)
        //{
        //    Subscription subscription = await db.Subscriptions.FindAsync(email);
        //    db.Subscriptions.Remove(subscription);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
