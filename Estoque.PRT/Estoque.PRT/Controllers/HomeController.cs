﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Taugor.PRT.Models.DBContext;
using Taugor.PRT.Models.Enums;

namespace Taugor.PRT.Controllers
{
    public class HomeController : Controller
    {
        private TaugorPRTContext db = new TaugorPRTContext();

        public ActionResult Index()
        {
            var cert = Request.ClientCertificate.Certificate;
            ViewBag.Products = db.Products
                .Where(k => k.Visibility == Visibility.Public)
                .ToList()
                .Where(k => !k.IsModule)
                .Take(4)
                .ToList();

            ViewBag.Versions = db.Versions
                .Where(k => k.Visibility == Visibility.Public)
                .OrderByDescending(k => k.ReleaseDate)
                .ToList()
                .Where(k => k.Released)
                .Take(8)
                .ToList();

            return View();
        }

        public ActionResult About()
        {
            
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}