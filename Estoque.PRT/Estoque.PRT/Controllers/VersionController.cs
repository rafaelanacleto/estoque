﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Taugor.PRT.Models;
using Prt = Taugor.PRT.Models;
using Taugor.PRT.Models.DBContext;

namespace Taugor.PRT.Controllers
{
    public class VersionController : Controller
    {
        private TaugorPRTContext db = new TaugorPRTContext();

        // GET: /Version/
        public async Task<ActionResult> Index()
        {
            var versions = db.Versions.Include(v => v.Product);
            return View(await versions.ToListAsync());
        }


        // GET : /Version/Product/5
        public ActionResult Product(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var modules = db.Products.Where(k => k.ProductId == id || k.ParentId == id)
                .Include(v => v.Versions);
            var versions = new List<Prt.Version>();

            modules.ToList().ForEach(k => versions.AddRange(k.Versions.Where(v => !v.IsRoadmap)));

            if (versions == null || versions.Count() == 0)
            {
                return HttpNotFound();
            }
            db.Products.Load();
            db.Changes.Load();

            return View(versions
                .OrderByDescending(k => k.ReleaseDate)
                .ThenByDescending(k => k.ReleasePlan)
                );
        }

        public ActionResult Module(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var versions = db.Versions.Where(k => k.ProductId == id)
                .Include(v => v.Product).ToList().Where(k => !k.IsRoadmap);
                //.Include(v => v.Changes)
                ;
            if (versions == null || versions.Count() == 0)
            {
                return HttpNotFound();
            }
            db.Products.Load();
            db.Changes.Load();

            return View(versions);
        }


        // GET: /Version/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var partial = id < 0;
            if (partial) { id *= -1; }
            Prt.Version version = await db.Versions.FindAsync(id);
            if (version == null)
            {
                return HttpNotFound();
            }
            db.Products.Load();
            db.Changes.Load();
            //ViewBag.Changes = version.Changes.Cast<Changelogs.Models.Change>();
            ViewBag.Partial = partial;
            return partial ? (ActionResult) PartialView(version) : View(version);
        }

        // GET: /Version/Create
        [Authorize]
        public ActionResult Create(int? ProductId = null)
        {
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "Name");
            if (ProductId.HasValue)
            {
                return View(new Prt.Version() { ProductId = ProductId.Value });
            }
            else
            {
                return View();
            }
            
        }

        // POST: /Version/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Create([Bind(Include = "VersionId,Type,FullVersion,ProductId,MajorVersion,MinorVersion,Build,Revision,Codename,TypeId,ReleaseDate,ReleasePlan,Author")] Prt.Version version)
        {
            if (ModelState.IsValid)
            {
                db.Versions.Add(version);
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { Id = version.VersionId });
            }

            ViewBag.ProductId = new SelectList(db.Products.Where(k => k.ParentId == null), "ProductId", "Name", version.ProductId);
            return View(version);
        }

        // GET: /Version/Edit/5
        [Authorize]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Prt.Version version = await db.Versions.FindAsync(id);
            if (version == null)
            {
                return HttpNotFound();
            }

            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "Name", version.ProductId);
            return View(version);
        }

        // POST: /Version/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Edit([Bind(Include = "VersionId,Type,FullVersion,ProductId,MajorVersion,MinorVersion,Build,Revision,Codename,TypeId,ReleaseDate,ReleasePlan,Author")] Prt.Version version)
        {
            if (ModelState.IsValid)
            {
                db.Entry(version).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { Id = version.VersionId });
            }
            ViewBag.ProductId = new SelectList(db.Products, "ProductId", "Name", version.ProductId);
            return View(version);
        }

        // GET: /Version/Delete/5
        [Authorize]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Prt.Version version = await db.Versions.FindAsync(id);
            db.Versions.Load();
            db.Changes.Load();
            //ViewBag.Changes = version.Changes.Cast<Changelogs.Models.Change>();
            if (version == null)
            {
                return HttpNotFound();
            }
            return View(version);
        }

        // POST: /Version/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Prt.Version version = await db.Versions.FindAsync(id);
            db.Versions.Load();
            db.Changes.Load();
            //ViewBag.Changes = version.Changes.Cast<Changelogs.Models.Change>();
            db.Versions.Remove(version);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
