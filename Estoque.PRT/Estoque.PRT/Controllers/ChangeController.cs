﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Taugor.PRT.Models;
using Taugor.PRT.Models.DBContext;

namespace Taugor.PRT.Controllers
{
    public class ChangeController : Controller
    {
        private TaugorPRTContext db = new TaugorPRTContext();

        // GET: /Change/
        public async Task<ActionResult> Index()
        {
            var changes = db.Changes.Include(c => c.Version).Include(c => c.Version.Product);
            return View(await changes.ToListAsync());
        }

        // GET: /Change/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            db.Products.Load();
            db.Versions.Load();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Change change = await db.Changes.FindAsync(id);
            if (change == null)
            {
                return HttpNotFound();
            }
            return View(change);
        }

        // GET: /Change/Create
        [Authorize]
        public ActionResult Create(int? VersionId = null)
        {
            db.Products.Load();
            db.Versions.Load();
            ViewBag.VersionId = new SelectList(db.Versions, "VersionId", "Reference", VersionId);
            if (VersionId.HasValue)
            {
                return View(new Change() { VersionId = VersionId.Value });
            }
            else
            {
                return View();
            }
            
        }

        // POST: /Change/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Create([Bind(Include = "ChangeId,Type,VersionId,VisibilityId,TypeId,Description,Date")] Change change)
        {
            if (ModelState.IsValid)
            {
                db.Changes.Add(change);
                await db.SaveChangesAsync();
                return RedirectToAction("Details", "Version", new { Id = change.VersionId });
            }

            ViewBag.VersionId = new SelectList(db.Versions, "VersionId", "FullVersion", change.VersionId);
            return View(change);
        }

        // GET: /Change/Edit/5
        [Authorize]
        public async Task<ActionResult> Edit(int? id)
        {
            db.Products.Load();
            db.Versions.Load();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Change change = await db.Changes.FindAsync(id);
            if (change == null)
            {
                return HttpNotFound();
            }
            ViewBag.VersionId = new SelectList(db.Versions, "VersionId", "FullVersion", change.VersionId);
            return View(change);
        }

        // POST: /Change/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Edit([Bind(Include = "ChangeId,Type,VersionId,VisibilityId,TypeId,Description,Date")] Change change)
        {
            if (ModelState.IsValid)
            {
                db.Entry(change).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Details", "Version", new { Id = change.VersionId });
            }
            ViewBag.VersionId = new SelectList(db.Versions, "VersionId", "FullVersion", change.VersionId);
            return View(change);
        }

        // GET: /Change/Delete/5
        [Authorize]
        public async Task<ActionResult> Delete(int? id)
        {
            db.Products.Load();
            db.Versions.Load();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Change change = await db.Changes.FindAsync(id);
            if (change == null)
            {
                return HttpNotFound();
            }
            return View(change);
        }

        // POST: /Change/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Change change = await db.Changes.FindAsync(id);
            db.Changes.Remove(change);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
