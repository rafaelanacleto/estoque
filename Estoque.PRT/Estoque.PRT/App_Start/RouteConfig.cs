﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Taugor.PRT
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "DefaultRoute",
                "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new { id = @"\d+" }
            );

            routes.MapRoute(
                "IdentifierRoute",
                "{controller}/{action}/{identifier}",
                new { controller = "Home", action = "Index", identifier = UrlParameter.Optional }
            );



            //routes.MapRoute(
            //    "RequestNoAction",
            //    "Request/{identifier}",
            //    new { controller = "Request", action = "Index" },
            //    new { identifier = new NotAnActionConstraint<RequestController>() { Action = "Details" } }
            //);

            //routes.MapRoute(
            //    "NoAction",
            //    "{controller}/{id}",
            //    new { controller = "Home", action = "Index", id = UrlParameter.Optional },
            //    new { id = @"\d+" }
            //);


            routes.MapMvcAttributeRoutes();
        }
    }
}
