﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.IO;

//namespace Taugor.PRT.Util
//{
//    public class LogReader
//    {
//        public IEnumerable<CL.Version> ParseVersion(string value)
//        {
//            var allVersions = new List<CL.Version>();

//            var valueVersions = value.Split(new[] { "VERSÃO:" }, StringSplitOptions.RemoveEmptyEntries);
//            foreach (var v in valueVersions)
//            {
//                CL.Version version = new CL.Version();
//                var lines = ("VERSÃO: " + v).Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
//                foreach (var line in lines)
//                {
//                    var tokens = line.Split(new[] { ':' });
//                    switch (tokens[0].ToUpper().Trim())
//                    {
//                        case "VERSÃO":
//                            var i = tokens[1].IndexOf('[');
//                            if (i != -1) { version.Type = CL.VersionType.Error; }
//                            var val = i == -1 ? tokens[1] : tokens[1].Substring(0, i);
//                            var versions = val.Split(new[] { '/' });
//                            version.FullVersion = versions.Length == 2 ? versions[1] : versions[0];
//                            break;
//                        case "DATA":
//                            version.ReleaseDate = Convert.ToDateTime(tokens[1].TrimStart().TrimEnd());
//                            break;
//                        case "AUTOR":
//                            version.Author = tokens[1].TrimStart().TrimEnd();
//                            break;
//                        case "PRODUTO":
//                            version.ProductName = tokens[1].TrimStart().TrimEnd();
//                            break;
//                        case "ERRO":
//                            break;
//                        case "DESCRIÇÃO":
//                        default:
//                            var len = tokens.Length;
//                            if (!string.IsNullOrEmpty(tokens[len - 1].TrimStart().TrimEnd()))
//                            {
//                                version.Changes.Add(new CL.Change()
//                                {
//                                    Description = tokens[len - 1].TrimStart().TrimEnd(),
//                                    Type = CL.ChangeType.Other,
//                                    Visibility = CL.Visibility.Public,
//                                });
//                            }
//                            break;
//                    }
//                }

//                allVersions.Add(version);
//                //VERSÂO:    1.0.0.0 / 1.0.7.4
//                //DATA:      18/03/2016
//                //AUTOR:     Romeu Junior
//                //DESCRIÇÃO: 1. Correção ao salvar apontamentos, verificatodos os valores (HN, HE e HFS) ao mesmo tempo na ordem "tarefa -> projeto -> recurso", agora verifica cada valor separadamente.                
//            }
//            return allVersions;
//        }



//        public CL.Change ParseChange(string value)
//        {
//            CL.Change change = new CL.Change();

//            StringReader sr = new StringReader(value);
//            string line = null;
//            do
//            {
//                line = sr.ReadLine();

//            } while (line != null);
//            //VERSÂO:    1.0.0.0 / 1.0.7.4
//            //DATA:      18/03/2016
//            //AUTOR:     Romeu Junior
//            //DESCRIÇÃO: 1. Correção ao salvar apontamentos, verificatodos os valores (HN, HE e HFS) ao mesmo tempo na ordem "tarefa -> projeto -> recurso", agora verifica cada valor separadamente.

//            return change;
//        }


//        public bool TryParseVersion(string value, out IEnumerable<CL.Version> versions)
//        {
//            try
//            {
//                versions = ParseVersion(value);
//                return true;
//            }
//            catch
//            {
//                versions = null;
//                return false;
//            }
//        }
//        public bool TryParseChange(string value, out CL.Change change)
//        {
//            try
//            {
//                change = ParseChange(value);
//                return change != null;
//            }
//            catch
//            {
//                change = null;
//                return false;
//            }
//        }
//    }
//}