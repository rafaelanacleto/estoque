﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Taugor.PRT.Models;

namespace Taugor.PRT.Util
{
    public static class MVCSelectHelper
    {

        public static IEnumerable<SelectListItem> GetSelectListItems<T>(Func<T, string> text, Func<T, string> value)
            where T : struct
        {
            return Enum.GetValues(typeof(T)).Cast<T>()
                                 .Select(c => new SelectListItem
                                 {
                                     Text = text(c),
                                     Value = value(c)
                                 });
        }

        public static IEnumerable<SelectListItem> GetSelectListItems<T>(Func<T, string> text)
            where T : struct
        {
            return GetSelectListItems<T>(
                text,
                v => (Convert.ToInt32(v)).ToString()
                );
        }

        public static IEnumerable<SelectListItem> GetSelectListItems<T>()
            where T : struct
        {
            return GetSelectListItems<T>(
                t => EnumHelper.GetDescriptionOrDefault(t),
                v => (Convert.ToInt32(v)).ToString()
                );
        }

        public static IEnumerable<SelectListItem> GetSelectListItems<T>(IEnumerable<T> items, Func<T, string> text, Func<T, string> value)
            where T : class
        {
            return items.Select(c => new SelectListItem
                                 {
                                     Text = text(c),
                                     Value = value(c)
                                 });
        }
        public static IEnumerable<SelectListItem> GetSelectListItems<T>(IEnumerable<T> items)
            where T : class, IEntity
        {
            return GetSelectListItems<T>(items, t => t.Reference, v => v.Key);
        }
        public static IEnumerable<SelectListItem> GetSelectListItems<T>(IEnumerable<T> items, Func<T, string> text)
            where T : class, IEntity
        {
            return GetSelectListItems<T>(items, text, v => v.Key);
        }


        public static SelectList GetSelectList<T>(string selectedValue = null)
            where T : struct
        {
            var list = GetSelectList(GetSelectListItems<T>(), selectedValue);
            return list;
        }

        public static SelectList GetSelectList<T>(Func<T, string> text, string selectedValue = null)
            where T : struct
        {
            return GetSelectList(GetSelectListItems<T>(text), selectedValue);
        }

        public static SelectList GetSelectList<T>(Func<T, string> text, Func<T, string> value, string selectedValue = null)
            where T : struct
        {
            return GetSelectList(GetSelectListItems<T>(text, value), selectedValue);
        }

        public static SelectList GetSelectList<T>(IEnumerable<T> items, Func<T, string> text, string selectedValue = null)
            where T : class, IEntity
        {
            return GetSelectList(GetSelectListItems<T>(items, text), selectedValue);
        }

        public static SelectList GetSelectList<T>(IEnumerable<T> items, Func<T, string> text, Func<T, string> value, string selectedValue = null)
             where T : class
        {
            return GetSelectList(GetSelectListItems<T>(items, text, value), selectedValue);
        }

        public static SelectList GetSelectList<T>(IEnumerable<T> items, string selectedValue = null)
        {
            IEnumerable<SelectListItem> listItems = null;
            var tType = items.GetItemType();
            if(tType == typeof(SelectListItem))
            {
                listItems = (IEnumerable<SelectListItem>)items;
            }
            else if (tType.IsClass && typeof(IEntity).IsAssignableFrom(tType))
            {
                listItems = GetSelectListItems(items.Cast<IEntity>());
            }
            else
            {
                return new SelectList(new SelectListItem[] { });
            }
            var list = string.IsNullOrEmpty(selectedValue)
                    ? new SelectList(listItems, "Value", "Text")
                    : new SelectList(listItems, "Value", "Text", selectedValue);

            return list;
                
        }


    }
}