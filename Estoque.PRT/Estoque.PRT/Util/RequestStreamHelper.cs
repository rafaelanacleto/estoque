﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Util
{
    public static class RequestStreamHelper
    {
        public static string GetQuery(HttpRequestBase request)
        {
            return request != null
                ? GetQuery(request.InputStream)
                : string.Empty;
        }
        public static string GetQuery(Stream stream)
        {
            if (stream != null)
            {
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                return new StreamReader(stream).ReadToEnd();
            }
            return string.Empty;
        }



        public static IEnumerable<T> GetTokens<T>(HttpRequestBase request, string key)
        {
            return GetTokens<T>(GetQuery(request), key);
        }
        public static IEnumerable<T> GetTokens<T>(Stream stream, string key)
        {
            return GetTokens<T>(GetQuery(stream), key);
        }
        public static IEnumerable<T> GetTokens<T>(string query, string key)
        {
            return query.Split(new[] { "&" }, StringSplitOptions.RemoveEmptyEntries)
                .Where(k => k.StartsWith(key)).Cast<T>();
        }


        public static Dictionary<string, List<string>> GetTokens(HttpRequestBase request)
        {
            return GetTokens(GetQuery(request));
        }
        public static Dictionary<string, List<string>> GetTokens(Stream stream)
        {
            return GetTokens(GetQuery(stream));
        }
        public static Dictionary<string, List<string>> GetTokens(string query)
        {
            var dictionary = new Dictionary<string, List<string>>();

            var aux = query.Split(new[] { "&" }, StringSplitOptions.RemoveEmptyEntries)
                .GroupBy(k => k.Substring(0, k.IndexOf('=')));

            foreach (var g in aux)
            {
                var list = dictionary[g.Key] = new List<string>();
                for (int i = 0; i < g.Count(); i++)
                {
                    var item = g.ElementAt(i);
                    list.Add(item.Substring(item.IndexOf('=') + 1));
                }
            }

            return dictionary;
        }
    }
}