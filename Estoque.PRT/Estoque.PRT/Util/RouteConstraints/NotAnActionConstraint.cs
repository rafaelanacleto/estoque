﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Routing;

namespace Taugor.PRT.Util.RouteConstraints
{
    public class NotAnActionConstraint<T> : IRouteConstraint
    {
        public string Action { get; set; }
        public bool Match(
            HttpContextBase httpContext,
            Route route,
            string parameterName,
            RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            var controller = typeof(T);
            if (values.ContainsKey(parameterName) && values[parameterName] != null)
            {
                if (controller.GetMethods().FirstOrDefault(k => k.Name == values[parameterName].ToString()) == null)
                {
                    values["action"] = Action;
                    return true;
                }
            }
            return false;
        }
    }
}
