﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Util
{
    public static class EnumHelper
    {
        public static string GetDescription<T>(T value)
            where T : struct
        {
            var attr = GetCustomAttribute<T, DescriptionAttribute>(value);
            return attr != null
                ? attr.Description
                : null;
        }

        public static string GetDescriptionOrDefault<T>(T value)
            where T : struct
        {
            return GetDescriptionOrDefault(value, value.ToString());
        }

        public static string GetDescriptionOrDefault<T>(T value, string defaultValue)
            where T : struct
        {
            var attr = GetCustomAttribute<T, DescriptionAttribute>(value);
            return attr != null
                ? attr.Description
                : defaultValue;
        }

        public static R GetCustomAttribute<T, R>(T value)
            where T : struct
            where R : Attribute
        {
            var type = typeof(T);
            var values = Enum.GetValues(type);
            var member = type.GetMember(value.ToString());
            var attribute = (R[])member[0].GetCustomAttributes(typeof(R), false);

            return attribute.Length > 0
                ? attribute[0]
                : null;
        }

        public static R[] GetCustomAttributes<T, R>(T value)
            where T : struct
            where R : Attribute
        {
            var type = typeof(T);
            var values = Enum.GetValues(type);
            var member = type.GetMember(value.ToString());
            var attributes = (R[])member[0].GetCustomAttributes(typeof(R), false);

            return attributes.Length > 0
                ? attributes
                : null;
        }

        public static int CountFlagsSetBits<T>(T value)
            where T : struct
        {
            if (typeof(T).GetCustomAttributes(typeof(FlagsAttribute), true).Length == 0)
            {
                return 0;
            }

            var i = Convert.ToInt32(value);
            var count = 0;
            while (i > 0)
            {
                if ((i & 1) == 1) { count++; }
                i = i >> 1;
            }
            return count;
        }

        public static int CountFlagsUnsetBits<T>(T value)
            where T : struct
        {
            var max = Convert.ToInt32(Enum.GetValues(typeof(T)).Cast<T>().Max());
            var bits = (int)Math.Log(max, 2);
            return bits - CountFlagsSetBits(value);
        }
    }
}