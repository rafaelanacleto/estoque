﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Taugor.PRT.Util
{
    public static class IEnumerableExtensions
    {
        public static Type GetItemType<T>(this IEnumerable<T> enumerable)
        {
            return typeof(T);
        }
    }
}