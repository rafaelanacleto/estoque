﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Taugor.PRT.Util
{
    public static class HtmlTagsHelper
    {
        public static string RemoveTags(string tag, string input)
        {
            return string.IsNullOrEmpty(input)
                ? string.Empty
                : Regex.Replace(
                    input,
                    string.Format(@"<{0}[^>]*>[\s\S]*?</{0}>", tag),
                    string.Empty,
                    RegexOptions.Singleline | RegexOptions.IgnoreCase
                );
        }

        public static string RemoveHtmlTags(this string input, string tag)
        {
            return RemoveTags(tag, input);
        }

        public static string FilterMessage(string message)
        {
            return message
                    .RemoveHtmlTags("script")
                    .RemoveHtmlTags("embed")
                    .RemoveHtmlTags("object")
                    .RemoveHtmlTags("frameset")
                    .RemoveHtmlTags("frame")
                    .RemoveHtmlTags("iframe")
                    .RemoveHtmlTags("meta")
                    .RemoveHtmlTags("link")
                    .RemoveHtmlTags("style");
        }
    }
}